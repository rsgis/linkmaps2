#LinkMaps2

This is a .NET Add-In for **ArcGIS 10.1** that allows you to synchronizes two map views.

The second window that the Add-In creates will only allow certain types of data and the symbology is hard-coded. Thus, the add-in is probably not very useful to others as it is, but it would be a good starting point for someone who wanted a similar tool and was willing to muck with the code.

##Installation

1. Close ArcMap if it is open.
2. Download [LinkMaps2.esriAddIn][1], double-click on it, and choose *Install Add-In* when prompted. 
3. If you do not see the toolbar, select *Toolbars* from the *Customize* menu and make sure the *LinkMaps2* toolbar is turned on.

##Usage

####Opening and closing the LinkMaps2 window

When you install the add-in, a new toolbar with only one button should be added to ArcMap. If you don't see it, choose *Toolbars* from the *Customize* menu and make sure that *LinkMaps2* is checked. Use the button on this toolbar to open and close the LinkMaps2 window.

![window][2]

####Adding layers to the LinkMaps2 window

Use the ![add][3] button on the LinkMaps2 window. 

You can add raster layers with 1 band or at least 5 bands. If the raster has one band then it will be displayed in black and white. If it has more, then it will be displayed with a 4,3,1 band combo.

You can also add polygon layers. They will be displayed as hollow with a yellow outline, along with annotation.

####Navigating around the map

Use the standard ArcMap nagivation tools in either the main ArcMap window or the LinkMaps2 window. The maps in the two windows will stay synchronized so that they have the same center point and display at the same scale. This could result in differing extents if the two windows are different sizes.

####Changing which raster displays

Only one raster will display at a time, and its name is shown at the top of the window. There are several ways you can choose which one is displayed.

Clicking on a raster in the Table of Contents will turn that layer on. 

You can also use the black arrows ![arrows][4] at the top of the LinkMaps2 window to show the previous or next raster in the list. 

Or you can use the keyboard arrows to show the previous or next rasters. The keyboard works if the map has focus (meaning it was the last thing you clicked in with your mouse) or if you click on the raster name at the top of the window.

You can also right-click on a raster layer in the Table of Contents and choose *Make visible raster*.

####Showing and hiding vector layers

Right-click on a vector layer in the Table of Contents and choose *Show* or *Hide*.

####Zooming to the extent of a layer

Right-click on the layer in the Table of Contents and choose *Zoom to layer*.

####Removing a layer

Right-click on the layer in the Table of Contents and choose *Remove layer*.

####Showing or hiding the Table of Contents

Use the ![toc][5] button at the top of the LinkMaps2 window.


[1]: https://bitbucket.org/rsgis/linkmaps2/downloads/LinkMaps2.esriAddIn
[2]: https://bitbucket.org/rsgis/linkmaps2/raw/39a742d29b47ce3625d1aa006eb21ddc4e32a499/doc_images/window.png
[3]: https://bitbucket.org/rsgis/linkmaps2/raw/39a742d29b47ce3625d1aa006eb21ddc4e32a499/doc_images/add.png
[4]: https://bitbucket.org/rsgis/linkmaps2/raw/39a742d29b47ce3625d1aa006eb21ddc4e32a499/doc_images/arrows.png
[5]: https://bitbucket.org/rsgis/linkmaps2/raw/39a742d29b47ce3625d1aa006eb21ddc4e32a499/doc_images/toc.png
