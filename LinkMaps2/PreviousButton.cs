﻿// -----------------------------------------------------------------------
// File: PreviousButton.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using System;

    using ESRI.ArcGIS.ADF.BaseClasses;

    using LinkMaps2.Properties;

    /// <summary>
    /// Button to change the visible raster to the one before the currently visible one.
    /// </summary>
    public class PreviousButton : BaseCommand
    {
        public PreviousButton()
        {
            base.m_caption = "Previous Raster";
            base.m_message = "Show previous raster layer.";
            base.m_bitmap = Resources.GenericBlackLeftArrowNoTail16;
        }

        public override bool Enabled
        {
            get { return LinkMaps2DockWin.RasterCount > 1; }
        }

        public override void OnClick()
        {
            LinkMaps2DockWin.ShowPreviousRaster();
        }

        public override void OnCreate(object hook)
        {
        }
    }
}