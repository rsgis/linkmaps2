﻿// -----------------------------------------------------------------------
// File: RemoveLayerCommand.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using ESRI.ArcGIS.ADF.BaseClasses;
    using ESRI.ArcGIS.Carto;
    using ESRI.ArcGIS.Controls;

    using LinkMaps2.Properties;

    /// <summary>
    /// Context menu command to remove the selected layer from the map.
    /// </summary>
    public class RemoveLayerCommand : BaseCommand
    {
        private ITOCControl toc;

        public RemoveLayerCommand()
        {
            base.m_caption = "Remove layer";
            base.m_bitmap = Resources.GenericBlackDelete16;
        }

        public override void OnClick()
        {
            var layer = (ILayer)this.toc.CustomProperty;
            if (layer is IRasterLayer)
            {
                LinkMaps2DockWin.RemoveRaster((IRasterLayer)layer);
            }
            else
            {
                LinkMaps2DockWin.RemoveVector((IFeatureLayer)layer);
            }
        }

        public override void OnCreate(object hook)
        {
            this.toc = (ITOCControl)hook;
        }
    }
}