﻿// -----------------------------------------------------------------------
// File: LinkMaps2Extension.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Windows.Forms;

    using ESRI.ArcGIS;
    using ESRI.ArcGIS.ArcMapUI;
    using ESRI.ArcGIS.Carto;
    using ESRI.ArcGIS.Controls;
    using ESRI.ArcGIS.Display;
    using ESRI.ArcGIS.esriSystem;
    using ESRI.ArcGIS.Framework;
    using ESRI.ArcGIS.Geometry;

    public class LinkMaps2Extension : ESRI.ArcGIS.Desktop.AddIns.Extension
    {
        /// <summary>
        /// Used for checking floating point equality.
        /// </summary>
        private const double Epsilon = 1E-12;

        /// <summary>
        /// The dockable window containing the map.
        /// </summary>
        private static IDockableWindow dockWindow;

        /// <summary>
        /// The extension object.
        /// </summary>
        private static LinkMaps2Extension extension;

        /// <summary>
        /// Center point that we're syncing everything to.
        /// </summary>
        private IPoint center;

        /// <summary>
        /// Indicates whether or not the document listeners have been initialized.
        /// </summary>
        private bool initialized;

        /// <summary>
        /// Indicates whether we're listening to document and item added to the active view events.
        /// If the dockable window is never opened, then there is no reason to listen.
        /// </summary>
        private bool listening;

        /// <summary>
        /// The focus map.
        /// </summary>
        private IMap mainMap;

        /// <summary>
        /// The active view.
        /// </summary>
        private IActiveView mainView;

        /// <summary>
        /// Scale that we're syncing everything to.
        /// </summary>
        private double scale;

        /// <summary>
        /// Indicates whether we're currently syncing the main map and dockable window.
        /// </summary>
        private bool syncing;

        /// <summary>
        /// Gets the LinkMaps2 extension.
        /// </summary>
        /// <returns></returns>
        internal static LinkMaps2Extension GetExtension()
        {
            if (LinkMaps2Extension.extension == null)
            {
                UID id = new UIDClass();
                id.Value = ThisAddIn.IDs.LinkMaps2Extension;
                ArcMap.Application.FindExtensionByCLSID(id);
            }

            return LinkMaps2Extension.extension;
        }

        /// <summary>
        /// Gets the LinkMaps2 dockable window containing the map.
        /// </summary>
        /// <returns>dockable window</returns>
        internal static IDockableWindow GetDockWindow()
        {
            if (LinkMaps2Extension.extension == null)
            {
                LinkMaps2Extension.GetExtension();
            }

            if (LinkMaps2Extension.dockWindow == null)
            {
                UID id = new UIDClass();
                id.Value = ThisAddIn.IDs.LinkMaps2DockWin;
                LinkMaps2Extension.dockWindow = ArcMap.DockableWindowManager.GetDockableWindow(id);
            }

            return LinkMaps2Extension.dockWindow;
        }

        /// <summary>
        /// Checks to see if we need to start or stop syncing, and does so if needed.
        /// </summary>
        /// <param name="mainPriority">true if we should initially sync to the main view, false if we
        /// should initially sync to the dockable window</param>
        internal void CheckSyncing(bool mainPriority = true)
        {
            if (!this.syncing && this.mainMap.LayerCount > 0 && LinkMaps2DockWin.LayerCount > 0)
            {
                this.StartSyncing(mainPriority);
            }
            else if (this.syncing
                     && (this.mainMap.LayerCount == 0 || LinkMaps2DockWin.LayerCount == 0))
            {
                this.StopSyncing();
            }
        }

        /// <summary>
        /// Turns listening on and off for document events and item added to the view events.
        /// </summary>
        /// <param name="listen">true if we should listen, false if not</param>
        internal void Listen(bool listen)
        {
            Debug.Print("Listen");

            if (this.listening == listen)
            {
                return;
            }

            this.listening = listen;
            if (listen)
            {
                this.mainMap = ArcMap.Document.FocusMap;
                this.mainView = ArcMap.Document.ActiveView;
                ((IActiveViewEvents_Event)this.mainMap).ItemAdded += this.ViewAddItem;
                //this.Initialize();
                this.CheckSyncing();
                if (!this.initialized)
                {
                    Debug.Print("Listen - Initialize");

                    var events = (IDocumentEvents_Event)ArcMap.Document;
                    events.NewDocument += this.DocumentNew;
                    events.CloseDocument += this.DocumentClose;
                    events.OpenDocument += this.DocumentOpen;
                    this.initialized = true;
                }
            }
            else
            {
                this.StopSyncing();
                ((IActiveViewEvents_Event)this.mainMap).ItemAdded -= this.ViewAddItem;
                this.mainView = null;
                this.mainMap = null;
                //this.Uninitialize();
            }
        }

        /// <summary>
        /// Centers the active view and zooms to a certain scale.
        /// </summary>
        /// <param name="center">point to zoom to</param>
        /// <param name="scale">scale to display at</param>
        private void CenterMainViewAt(IPoint center, double scale)
        {
            IEnvelope extent = this.mainView.Extent.Envelope;
            extent.CenterAt(center);
            this.mainView.Extent = extent;
            this.mainMap.MapScale = scale;
            this.mainView.Refresh();
        }

        /// <summary>
        /// Checks to see if a center point and display scale match the ones stored in the extension.
        /// </summary>
        /// <param name="center">center point to compare</param>
        /// <param name="scale">scale to compare</param>
        /// <returns>true if equal, false if not</returns>
        private bool SyncedToExtension(IPoint center, double scale)
        {
            return (Math.Abs(scale - this.scale) < LinkMaps2Extension.Epsilon)
                   && (this.center != null)
                   && ((IClone)center).IsEqual((IClone)this.center);
        }

        /// <summary>
        /// Start syncing the main view and the dockable window.
        /// </summary>
        /// <param name="mainPriority">true if the main view should have priority for settings</param>
        private void StartSyncing(bool mainPriority)
        {
            Debug.Print("StartSyncing");

            if (this.syncing)
            {
                return;
            }

            if (LinkMaps2DockWin.SetSpatialReference(this.mainMap.SpatialReference) || mainPriority)
            {
                // First sync to the main view if it has priority or we had to change the spatial
                // reference on the dockable window's map
                this.center = ((IArea)this.mainView.Extent).Centroid;
                this.scale = this.mainMap.MapScale;
                LinkMaps2DockWin.CenterAt(this.center, this.scale);
            }
            else
            {
                // First sync to the dockable window if the spatial reference was already correct and
                // it has priority
                this.center = LinkMaps2DockWin.Center;
                this.scale = LinkMaps2DockWin.MapScale;
                this.CenterMainViewAt(this.center, this.scale);
            }

            // Set the listeners
            var events = (IActiveViewEvents_Event)this.mainMap;
            events.AfterDraw += this.ViewAfterDraw;
            events.ItemDeleted += this.ViewDeleteItem;
            events.SpatialReferenceChanged += this.ViewSpatialReferenceChange;
            LinkMaps2DockWin.StartSyncing(this.LinkedMapAfterDraw);
            this.syncing = true;
        }

        /// <summary>
        /// Stop syncing the main view and dockable window.
        /// </summary>
        private void StopSyncing()
        {
            Debug.Print("StopSyncing");

            if (!this.syncing)
            {
                return;
            }

            // Remove the listeners
            var events = (IActiveViewEvents_Event)this.mainMap;
            events.AfterDraw -= this.ViewAfterDraw;
            events.ItemDeleted -= this.ViewDeleteItem;
            events.SpatialReferenceChanged -= this.ViewSpatialReferenceChange;
            LinkMaps2DockWin.StopSyncing(this.LinkedMapAfterDraw);
            this.syncing = false;
        }

        /// <summary>
        /// Runs when the map document is closed. Removes listeners on and references to the main
        /// map and active view.
        /// </summary>
        private void DocumentClose()
        {
            Debug.Print("DocumentClose");

            LinkMaps2DockWin.Clear();
            this.Listen(false);
        }

        /// <summary>
        /// Runs when a new map document is created. Adds listeners on and references to the main
        /// map and active view.
        /// </summary>
        private void DocumentNew()
        {
            Debug.Print("DocumentNew");

            this.Listen(true);
        }

        /// <summary>
        /// Runs when an existing map document is opened. Adds listeners on and references to the main
        /// map and active view.
        /// </summary>
        private void DocumentOpen()
        {
            Debug.Print("DocumentOpen");

            this.Listen(true);
        }

        /// <summary>
        /// Runs when a new layer is added to the main view. Makes sure we start syncing if needed.
        /// </summary>
        /// <param name="item"></param>
        private void ViewAddItem(object item)
        {
            this.CheckSyncing(false);
        }

        /// <summary>
        /// Runs when a layer is deleted from the main view. Makes sure we stop syncing if needed.
        /// </summary>
        /// <param name="item"></param>
        private void ViewDeleteItem(object item)
        {
            this.CheckSyncing();
        }

        /// <summary>
        /// Runs when the main view's spatial reference is changed. Changes the spatial reference of
        /// the dockable window to match.
        /// </summary>
        private void ViewSpatialReferenceChange()
        {
            if (LinkMaps2DockWin.SetSpatialReference(this.mainMap.SpatialReference))
            {
                LinkMaps2DockWin.CenterAt(this.center, this.scale);
            }
        }

        /// <summary>
        /// Runs after the main view has finished drawing. Makes sure the active view and dockable
        /// window's map are in sync.
        /// </summary>
        /// <param name="display"></param>
        /// <param name="phase"></param>
        private void ViewAfterDraw(IDisplay display, esriViewDrawPhase phase)
        {
            // Don't need to do anything if in the wrong draw phase
            if (phase != esriViewDrawPhase.esriViewInitialized)
            {
                return;
            }

            // Get settings for active view
            IPoint center = ((IArea)this.mainView.Extent).Centroid;
            double scale = this.mainMap.MapScale;

            // Don't need to do anything if the view is already synced to the extension
            if (this.SyncedToExtension(center, scale))
            {
                return;
            }

            // Save the view's settings to the extension and sync the dockable window's map to them
            this.center = center;
            this.scale = scale;
            LinkMaps2DockWin.CenterAt(this.center, this.scale);
        }

        /// <summary>
        /// Runs after the map in the dockable window has finished drawing. Makes sure the active
        /// view and dockable window's map are in sync.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LinkedMapAfterDraw(object sender, IMapControlEvents2_OnAfterDrawEvent e)
        {
            // Don't need to do anything if in the wrong draw phase
            if ((esriViewDrawPhase)e.viewDrawPhase != esriViewDrawPhase.esriViewInitialized)
            {
                return;
            }

            // Get settings for the map control
            IPoint center = LinkMaps2DockWin.Center;
            double scale = LinkMaps2DockWin.MapScale;

            // Don't need to do anything if the map is already synced to the extension
            if (this.SyncedToExtension(center, scale))
            {
                return;
            }

            // Save the map's settings to the extension and sync the active view to them
            this.center = center;
            this.scale = scale;
            this.CenterMainViewAt(this.center, this.scale);
        }

        /// <summary>
        /// Runs when an existing map document that has serialized info for this extension is opened.
        /// </summary>
        /// <param name="inStrm"></param>
        protected override void OnLoad(Stream inStrm)
        {
            Debug.Print("OnLoad");

            base.OnLoad(inStrm);

            LinkMaps2Extension.GetDockWindow().Show(true);
            this.Listen(true);
            Application.DoEvents();

            try
            {
                char sep = '|';
                var formatter = new BinaryFormatter();
                var info = (Dictionary<string, string>)formatter.Deserialize(inStrm);
                
                var paths = new List<string>(info["Rasters"].Split(sep));
                paths.RemoveAll(string.IsNullOrEmpty);
                for (int i = paths.Count - 1; i >= 0; i--)
                {
                    AddLayerButton.AddRaster(paths[i]);
                }

                paths = new List<string>(info["Vectors"].Split(sep));
                paths.RemoveAll(string.IsNullOrEmpty);
                for (int i = paths.Count - 1; i >= 0; i--)
                {
                    AddLayerButton.AddVector(paths[i]);
                }

                // Need to see if these keys exist because they don't in the first version of LinkMaps
                if (info.ContainsKey("VisibleRaster"))
                {
                    var visibilities = new List<string>(info["VectorVisibilities"].Split(sep));
                    visibilities.RemoveAll(string.IsNullOrEmpty);
                    for (int i = 0; i < visibilities.Count; i++)
                    {
                        if (!bool.Parse(visibilities[i]))
                        {
                            LinkMaps2DockWin.ToggleVisibility(i);
                        }
                    }

                    LinkMaps2DockWin.ShowRaster(int.Parse(info["VisibleRaster"]));
                }

                this.center = new PointClass
                {
                    X = double.Parse(info["X"]),
                    Y = double.Parse(info["Y"])
                };
                this.scale = double.Parse(info["Scale"]);
                LinkMaps2DockWin.CenterAt(this.center, this.scale);
                LinkMaps2Extension.GetExtension().CheckSyncing();
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    "Unable to load LinkMaps2 settings: " + e.Message,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Runs when the map document is saved. If there is stuff in the dockable window then
        /// serialize it so it can be saved with the document. The formatter seems to be only able
        /// to correctly serialize and deserialize a dictionary of strings.
        /// </summary>
        /// <param name="outStrm"></param>
        protected override void OnSave(Stream outStrm)
        {
            Debug.Print("OnSave");

            // Nothing to do if there are no layers in the dockable window
            if (!LinkMaps2DockWin.Initialized || LinkMaps2DockWin.LayerCount == 0)
            {
                return;
            }

            // Find out if we should save info if the dockable window is closed
            if (!LinkMaps2Extension.GetDockWindow().IsVisible())
            {
                string msg = "There are layers in the LinkMaps2 window. Do you want to save them"
                             + " with this map document?";
                if (MessageBox.Show(
                    msg, "LinkMaps2", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    == DialogResult.No)
                {
                    LinkMaps2DockWin.Clear();
                    return;
                }
            }

            try
            {
                string sep = "|";
                var info = new Dictionary<string, string>();
                info.Add("X", LinkMaps2DockWin.Center.X.ToString(CultureInfo.InvariantCulture));
                info.Add("Y", LinkMaps2DockWin.Center.Y.ToString(CultureInfo.InvariantCulture));
                info.Add("Scale", LinkMaps2DockWin.MapScale.ToString(CultureInfo.InvariantCulture));
                info.Add("Rasters", string.Join(sep, LinkMaps2DockWin.RasterPaths.ToArray()));
                info.Add("VisibleRaster", LinkMaps2DockWin.VisibleRasterIndex.ToString());
                info.Add("Vectors", string.Join(sep, LinkMaps2DockWin.VectorPaths.ToArray()));
                string vectors = "";
                foreach (bool v in LinkMaps2DockWin.VectorVisibilities)
                {
                    vectors = string.Format("{0}{1}{2}", vectors, sep, v);
                }
                info.Add("VectorVisibilities", vectors);
                var formatter = new BinaryFormatter();
                formatter.Serialize(outStrm, info);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    "Unable to save LinkMaps2 settings: " + e.Message,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            base.OnSave(outStrm);
        }

        /// <summary>
        /// Runs when the extension starts up.
        /// </summary>
        protected override void OnStartup()
        {
            Debug.Print("OnStartup");

            // Bind to desktop license
            if (!RuntimeManager.Bind(ProductCode.Desktop))
            {
                MessageBox.Show(
                    "Error getting Esri license for LinkMaps Add-In!",
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            LinkMaps2Extension.extension = this;
        }

        /// <summary>
        /// Runs when the extension shuts down. Don't need to run Unitialize() because it should've
        /// already gotten called in the document close handler.
        /// </summary>
        protected override void OnShutdown()
        {
            Debug.Print("OnShutdown");

            if (this.listening)
            {
                this.Listen(false);
            }
            LinkMaps2Extension.dockWindow = null;
            LinkMaps2Extension.extension = null;

            // Remove the document listeners
            if (this.initialized)
            {
                var events = (IDocumentEvents_Event)ArcMap.Document;
                events.NewDocument -= this.DocumentNew;
                events.CloseDocument -= this.DocumentClose;
                events.OpenDocument -= this.DocumentOpen;
            }

            base.OnShutdown();
        }
    }
}