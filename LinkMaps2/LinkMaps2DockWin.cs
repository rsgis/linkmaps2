﻿// -----------------------------------------------------------------------
// File: LinkMaps2DockWin.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using ESRI.ArcGIS.Carto;
    using ESRI.ArcGIS.Controls;
    using ESRI.ArcGIS.Geometry;
    using ESRI.ArcGIS.SystemUI;
    using ESRI.ArcGIS.esriSystem;

    /// <summary>
    /// Designer class of the dockable window add-in. It contains user interfaces that
    /// make up the dockable window.
    /// </summary>
    public partial class LinkMaps2DockWin : UserControl
    {
        /// <summary>
        /// Map control.
        /// </summary>
        private static AxMapControl map;

        /// <summary>
        /// Holds the file paths for each layer.
        /// </summary>
        private static Dictionary<ILayer, string> paths;
 
        /// <summary>
        /// Number of raster layers in the map.
        /// </summary>
        private static int rasterCount;

        /// <summary>
        /// The split container for toggling the TOC.
        /// </summary>
        private static SplitContainer splitPane;

        /// <summary>
        /// TOC control.
        /// </summary>
        private static AxTOCControl toc;

        /// <summary>
        /// Number of vector layers in the map.
        /// </summary>
        private static int vectorCount;

        /// <summary>
        /// Index of the visible raster layer.
        /// </summary>
        private static int visibleRaster;

        /// <summary>
        /// Label to show the name of the visible raster.
        /// </summary>
        private static Label visibleRasterLabel;

        /// <summary>
        /// Label to show a message to the user.
        /// </summary>
        private static Label message;

        /// <summary>
        /// Map context menu for TOC.
        /// </summary>
        private IToolbarMenu mapContextMenu;

        /// <summary>
        /// Layer context menu for TOC.
        /// </summary>
        private IToolbarMenu layerContextMenu;

        /// <summary>
        /// Layer visibility command on context menu. Need it so we can change its icon.
        /// </summary>
        private LayerVisibilityCommand visibilityCommand;

        public LinkMaps2DockWin(object hook)
        {
            InitializeComponent();
            this.Hook = hook;
        }

        /// <summary>
        /// Host object of the dockable window
        /// </summary>
        private object Hook
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the center point of the map.
        /// </summary>
        internal static IPoint Center
        {
            get { return ((IArea)LinkMaps2DockWin.map.Extent).Centroid; }
        }

        /// <summary>
        /// Gets a value indicating whether or not the dockable window has been initialized.
        /// </summary>
        internal static bool Initialized { get; private set; }

        /// <summary>
        /// Gets the number of layers in the map.
        /// </summary>
        internal static int LayerCount
        {
            get { return LinkMaps2DockWin.map.LayerCount; }
        }

        /// <summary>
        /// Gets the map's display scale.
        /// </summary>
        internal static double MapScale
        {
            get { return LinkMaps2DockWin.map.MapScale; }
        }

        /// <summary>
        /// Gets the number of raster layers in the map.
        /// </summary>
        internal static double RasterCount
        {
            get { return LinkMaps2DockWin.rasterCount; }
        }

        /// <summary>
        /// Gets an ordered list of file paths for each of the raster layers in the map.
        /// </summary>
        internal static List<string> RasterPaths
        {
            get
            {
                var info = new List<string>();
                for (int i = LinkMaps2DockWin.vectorCount; i < LinkMaps2DockWin.map.LayerCount; i++)
                {
                    info.Add(LinkMaps2DockWin.paths[LinkMaps2DockWin.map.get_Layer(i)]);
                }

                return info;
            }
        }

        /// <summary>
        /// Gets an ordered list of file paths for each of the vector layers in the map.
        /// </summary>
        internal static List<string> VectorPaths
        {
            get
            {
                var info = new List<string>();
                for (int i = 0; i < LinkMaps2DockWin.vectorCount; i++)
                {
                    info.Add(LinkMaps2DockWin.paths[LinkMaps2DockWin.map.get_Layer(i)]);
                }

                return info;
            }
        }

        /// <summary>
        /// Gets an ordered list of file paths for each of the vector layers in the map.
        /// </summary>
        internal static List<bool> VectorVisibilities
        {
            get
            {
                var info = new List<bool>();
                for (int i = 0; i < LinkMaps2DockWin.vectorCount; i++)
                {
                    info.Add(LinkMaps2DockWin.map.get_Layer(i).Visible);
                }

                return info;
            }
        }

        /// <summary>
        /// Gets the index of the visible raster.
        /// </summary>
        internal static int VisibleRasterIndex
        {
            get { return LinkMaps2DockWin.visibleRaster; }
        }

        /// <summary>
        /// Initialize static variables.
        /// </summary>
        private static void Initialize()
        {
            LinkMaps2DockWin.paths = new Dictionary<ILayer, string>();
            LinkMaps2DockWin.visibleRaster = -1;
            LinkMaps2DockWin.rasterCount = 0;
            LinkMaps2DockWin.vectorCount = 0;
            LinkMaps2DockWin.visibleRasterLabel.Text = "";
            LinkMaps2DockWin.Initialized = true;
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            // Set up toolbar
            esriCommandStyles style = esriCommandStyles.esriCommandStyleIconOnly;
            this.linkToolbarControl.SetBuddyControl(this.mapControl);
            this.linkToolbarControl.AddItem(new ToggleTocButton());
            this.linkToolbarControl.AddItem(new AddLayerButton(), 0, -1, true, 0, style);
            this.linkToolbarControl.AddItem(
                "esriControls.ControlsMapZoomInTool", 0, -1, true, 0, style);
            this.linkToolbarControl.AddItem("esriControls.ControlsMapZoomOutTool");
            this.linkToolbarControl.AddItem("esriControls.ControlsMapPanTool");
            this.linkToolbarControl.AddItem("esriControls.ControlsMapZoomToLastExtentBackCommand");
            this.linkToolbarControl.AddItem("esriControls.ControlsMapZoomToLastExtentForwardCommand");
            this.linkToolbarControl.AddItem("esriControls.ControlsMapFullExtentCommand");
            this.linkToolbarControl.AddItem(new PreviousButton(), 0, -1, true, 0, style);
            this.linkToolbarControl.AddItem(new NextButton());
            this.messageLabel.Location = this.currentRasterLabel.Location;

            // Set up TOC
            this.tocControl.SetBuddyControl(this.mapControl);
            this.layerContextMenu = new ToolbarMenuClass();
            this.layerContextMenu.SetHook(this.tocControl);
            this.visibilityCommand = new LayerVisibilityCommand();
            this.layerContextMenu.AddItem(this.visibilityCommand);
            this.layerContextMenu.AddItem(new ZoomToLayerCommand());
            this.layerContextMenu.AddItem(new RemoveLayerCommand());
            this.mapContextMenu = new ToolbarMenuClass();
            this.mapContextMenu.AddItem(new ClearCommand());

            // Set static variables and make sure the extension gets initialized in case this is
            // opening with ArcMap
            LinkMaps2DockWin.map = this.mapControl;
            LinkMaps2DockWin.toc = this.tocControl;
            LinkMaps2DockWin.splitPane = this.linkSplitContainer;
            LinkMaps2DockWin.visibleRasterLabel = this.currentRasterLabel;
            LinkMaps2DockWin.message = this.messageLabel;
            LinkMaps2DockWin.Initialize();
            LinkMaps2Extension.GetExtension().Listen(true);
        }

        /// <summary>
        /// Adds a raster layer to the map.
        /// </summary>
        /// <param name="layer">layer to add</param>
        /// <param name="path">full file path to the dataset</param>
        internal static void AddRaster(IRasterLayer layer, string path)
        {
            LinkMaps2DockWin.HideRaster();
            ((ILegendInfo)layer).LegendGroup[0].Visible = false;
            LinkMaps2DockWin.map.AddLayer(layer, LinkMaps2DockWin.vectorCount);
            LinkMaps2DockWin.ShowRaster(LinkMaps2DockWin.vectorCount);
            LinkMaps2DockWin.RefreshMap();
            LinkMaps2DockWin.paths.Add(layer, path);
            LinkMaps2DockWin.rasterCount++;
            LinkMaps2Extension.GetExtension().CheckSyncing(true);
        }

        /// <summary>
        /// Adds a vector layer to the map.
        /// </summary>
        /// <param name="layer">layer to add</param>
        /// <param name="path">full file path to the dataset</param>
        internal static void AddVector(IFeatureLayer layer, string path)
        {
            ((ILegendInfo)layer).LegendGroup[0].Visible = false;
            LinkMaps2DockWin.map.AddLayer(layer);
            LinkMaps2DockWin.RefreshMap();
            LinkMaps2DockWin.paths.Add(layer, path);
            LinkMaps2DockWin.vectorCount++;
            if (LinkMaps2DockWin.visibleRaster != -1)
            {
                LinkMaps2DockWin.visibleRaster++;
            }
            LinkMaps2Extension.GetExtension().CheckSyncing(true);
        }

        /// <summary>
        /// Center the map and zoom to a certain scale.
        /// </summary>
        /// <param name="center">point to zoom to</param>
        /// <param name="scale">scale to display at</param>
        internal static void CenterAt(IPoint center, double scale)
        {
            LinkMaps2DockWin.map.CenterAt(center);
            LinkMaps2DockWin.map.MapScale = scale;
            LinkMaps2DockWin.RefreshMap();
        }

        /// <summary>
        /// Clears everything out of the dockable window.
        /// </summary>
        internal static void Clear()
        {
            if (LinkMaps2DockWin.map != null)
            {
                LinkMaps2DockWin.Initialize();
                LinkMaps2DockWin.map.ClearLayers();
                LinkMaps2Extension.GetExtension().CheckSyncing();
                LinkMaps2DockWin.RefreshMap();
            }
        }

        /// <summary>
        /// Hides the currently visible raster.
        /// </summary>
        private static void HideRaster()
        {
            if (LinkMaps2DockWin.visibleRaster == -1)
            {
                return;
            }

            LinkMaps2DockWin.map.get_Layer(LinkMaps2DockWin.visibleRaster).Visible = false;
            LinkMaps2DockWin.visibleRasterLabel.Text = "";
            LinkMaps2DockWin.visibleRaster = -1;
            LinkMaps2DockWin.RefreshMap();
        }

        /// <summary>
        /// Refreshes the map and TOC.
        /// </summary>
        internal static void RefreshMap()
        {
            LinkMaps2DockWin.map.Refresh();
            LinkMaps2DockWin.toc.Update();
        }

        /// <summary>
        /// Removes a raster layer from the map.
        /// </summary>
        /// <param name="layer">layer to remove</param>
        internal static void RemoveRaster(IRasterLayer layer)
        {
            // Look for the layer to remove
            for (int i = LinkMaps2DockWin.vectorCount; i < LinkMaps2DockWin.map.LayerCount; i++)
            {
                // Go to the next layer if this isn't the right one
                if (LinkMaps2DockWin.map.get_Layer(i) != layer)
                {
                    continue;
                }

                // If we're removing the visible raster, then if it's the only raster just hide it so
                // things get cleared out, but otherwise switch to the previous raster
                if (LinkMaps2DockWin.rasterCount == 1)
                {
                    LinkMaps2DockWin.HideRaster();
                }
                else if (i == LinkMaps2DockWin.visibleRaster)
                {
                    LinkMaps2DockWin.ShowPreviousRaster();
                }

                // Decrement the visible raster index if we're removing a raster before the currently
                // visible one
                if (i < LinkMaps2DockWin.visibleRaster)
                {
                    LinkMaps2DockWin.visibleRaster--;
                }

                // Now actually delete the layer
                LinkMaps2DockWin.map.DeleteLayer(i);
                LinkMaps2DockWin.rasterCount--;
                LinkMaps2Extension.GetExtension().CheckSyncing();
            }
        }

        /// <summary>
        /// Removes a vector layer from the map.
        /// </summary>
        /// <param name="layer">layer to remove</param>
        internal static void RemoveVector(IFeatureLayer layer)
        {
            LinkMaps2DockWin.map.Map.DeleteLayer(layer);
            LinkMaps2DockWin.vectorCount--;
            LinkMaps2DockWin.visibleRaster--;
            LinkMaps2Extension.GetExtension().CheckSyncing();
        }

        /// <summary>
        /// Sets the spatial reference for the map.
        /// </summary>
        /// <param name="sr">spatial reference to use</param>
        /// <returns>true if the spatial reference was changed, false if not</returns>
        internal static bool SetSpatialReference(ISpatialReference sr)
        {
            if (LinkMaps2DockWin.map.SpatialReference != null
                && ((IClone)LinkMaps2DockWin.map.SpatialReference).IsEqual((IClone)sr))
            {
                return false;
            }
            else
            {
                LinkMaps2DockWin.map.SpatialReference = sr;
                return true;
            }
        }

        /// <summary>
        /// Shows a message on the dockable window.
        /// </summary>
        /// <param name="msg">string to show, or null if none</param>
        internal static void ShowMessage(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                LinkMaps2DockWin.message.Text = "";
                LinkMaps2DockWin.message.Visible = false;
            }
            else
            {
                LinkMaps2DockWin.message.Text = msg;
                LinkMaps2DockWin.message.Visible = true;
            }

            Application.DoEvents();
        }

        /// <summary>
        /// Makes the next raster layer visible.
        /// </summary>
        internal static void ShowNextRaster()
        {
            if (LinkMaps2DockWin.rasterCount < 2)
            {
                return;
            }

            if (LinkMaps2DockWin.visibleRaster == LinkMaps2DockWin.map.LayerCount - 1)
            {
                LinkMaps2DockWin.ShowRaster(LinkMaps2DockWin.vectorCount);
            }
            else
            {
                LinkMaps2DockWin.ShowRaster(LinkMaps2DockWin.visibleRaster + 1);
            }
        }

        /// <summary>
        /// Makes the previous raster layer visible.
        /// </summary>
        internal static void ShowPreviousRaster()
        {
            if (LinkMaps2DockWin.rasterCount < 2)
            {
                return;
            }

            if (LinkMaps2DockWin.visibleRaster == LinkMaps2DockWin.vectorCount)
            {
                LinkMaps2DockWin.ShowRaster(LinkMaps2DockWin.map.LayerCount - 1);
            }
            else
            {
                LinkMaps2DockWin.ShowRaster(LinkMaps2DockWin.visibleRaster - 1);
            }
        }

        /// <summary>
        /// Makes a specific raster layer visible.
        /// </summary>
        /// <param name="i">index of the layer to show</param>
        internal static void ShowRaster(int i)
        {
            if (LinkMaps2DockWin.visibleRaster != -1)
            {
                LinkMaps2DockWin.map.get_Layer(LinkMaps2DockWin.visibleRaster).Visible = false;
            }

            LinkMaps2DockWin.map.get_Layer(i).Visible = true;
            LinkMaps2DockWin.visibleRasterLabel.Text = LinkMaps2DockWin.map.get_Layer(i).Name;
            LinkMaps2DockWin.visibleRaster = i;
            LinkMaps2DockWin.RefreshMap();
        }

        /// <summary>
        /// Makes a specific raster layer visible. The layer must already be in the map.
        /// </summary>
        /// <param name="layer">layer to show</param>
        internal static void ShowRaster(IRasterLayer layer)
        {
            for (int i = LinkMaps2DockWin.vectorCount; i < LinkMaps2DockWin.map.LayerCount; i++)
            {
                if (LinkMaps2DockWin.map.get_Layer(i) == layer)
                {
                    LinkMaps2DockWin.ShowRaster(i);
                    return;
                }
            }
        }

        /// <summary>
        /// Starts syncing, using the provided handler for after the map redraws.
        /// </summary>
        /// <param name="handler">after draw handler</param>
        internal static void StartSyncing(IMapControlEvents2_Ax_OnAfterDrawEventHandler handler)
        {
            LinkMaps2DockWin.map.OnAfterDraw += handler;
        }

        /// <summary>
        /// Stops syncing by removing the provided handler for after the map redraws.
        /// </summary>
        /// <param name="handler">after draw handler</param>
        internal static void StopSyncing(IMapControlEvents2_Ax_OnAfterDrawEventHandler handler)
        {
            LinkMaps2DockWin.map.OnAfterDraw -= handler;
        }

        /// <summary>
        /// Toggles the visibility of the TOC.
        /// </summary>
        internal static void ToggleToc()
        {
            LinkMaps2DockWin.splitPane.Panel1Collapsed = !LinkMaps2DockWin.splitPane.Panel1Collapsed;
        }

        /// <summary>
        /// Toggles the visibility of a layer. If it's a raster layer and is already visible, then
        /// nothing changes.
        /// </summary>
        /// <param name="i">index of the layer to toggle</param>
        internal static void ToggleVisibility(int i)
        {
            var layer = LinkMaps2DockWin.map.get_Layer(i);
            if (layer is IRasterLayer && !layer.Visible)
            {
                LinkMaps2DockWin.ShowRaster((IRasterLayer)layer);
            }
            else if (layer is IFeatureLayer)
            {
                layer.Visible = !layer.Visible;
                LinkMaps2DockWin.RefreshMap();
            }
        }

        /// <summary>
        /// Toggles the visibility of a layer. If it's a raster layer and is already visible, then
        /// nothing changes.
        /// </summary>
        /// <param name="layer">layer to toggle</param>
        internal static void ToggleVisibility(ILayer layer)
        {
            if (layer is IRasterLayer && !layer.Visible)
            {
                LinkMaps2DockWin.ShowRaster((IRasterLayer)layer);
            }
            else if (layer is IFeatureLayer)
            {
                layer.Visible = !layer.Visible;
                LinkMaps2DockWin.RefreshMap();
            }
        }

        /// <summary>
        /// Zooms to a specific extent.
        /// </summary>
        /// <param name="extent">extent to use</param>
        internal static void ZoomToExtent(IEnvelope extent)
        {
            LinkMaps2DockWin.map.Extent = extent;
        }

        /// <summary>
        /// Runs when the user clicks on the label with the current raster name. Gives the map the
        /// focus so that the arrow keys work to switch between rasters.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void currentRasterLabel_Click(object sender, EventArgs e)
        {
            this.mapControl.Focus();
        }

        /// <summary>
        /// Runs when the dockable window is hidden (I have no idea why it doesn't also run when it
        /// becomes visible).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LinkMaps2DockWin_VisibleChanged(object sender, EventArgs e)
        {
            LinkMaps2Extension.GetExtension().Listen(false);
        }

        /// <summary>
        /// Runs after a key is pressed when the map control has the focus. Displays a different
        /// raster if the key was an arrow key.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mapControl_OnKeyUp(object sender, IMapControlEvents2_OnKeyUpEvent e)
        {
            if (e.keyCode == 38 || e.keyCode == 39)
            {
                LinkMaps2DockWin.ShowNextRaster();
            }
            else if (e.keyCode == 37 || e.keyCode == 40)
            {
                LinkMaps2DockWin.ShowPreviousRaster();
            }
        }

        /// <summary>
        /// Runs when the mouse is clicked in the TOC. Left-clicks turn a raster layer on, and right-
        /// clicks show a context menu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tocControl_OnMouseDown(object sender, ITOCControlEvents_OnMouseDownEvent e)
        {
            if (!(e.button == 1 || e.button == 2))
            {
                return;
            }

            IBasicMap map = null;
            ILayer layer = null;
            object other = null;
            object index = null;
            var item = esriTOCControlItem.esriTOCControlItemNone;
            this.tocControl.HitTest(e.x, e.y, ref item, ref map, ref layer, ref other, ref index);
            if (
                !(item == esriTOCControlItem.esriTOCControlItemLayer
                  || item == esriTOCControlItem.esriTOCControlItemMap))
            {
                return;
            }

            if (item == esriTOCControlItem.esriTOCControlItemMap && e.button == 2)
            {
                // If a map was right-clicked on
                this.mapContextMenu.PopupMenu(e.x, e.y, this.tocControl.hWnd);
            }
            else if (e.button == 1 && layer is IRasterLayer)
            {
                // If a raster layer was left-clicked on
                LinkMaps2DockWin.ShowRaster((IRasterLayer)layer);
            }
            else if (e.button == 2)
            {
                // If a layer was right-clicked on
                this.tocControl.SelectItem(layer, null);
                this.tocControl.CustomProperty = layer;
                this.visibilityCommand.UpdateBitmap();
                this.layerContextMenu.PopupMenu(e.x, e.y, this.tocControl.hWnd);
            }
        }

        /// <summary>
        /// Implementation class of the dockable window add-in. It is responsible for 
        /// creating and disposing the user interface class of the dockable window.
        /// </summary>
        public class AddinImpl : ESRI.ArcGIS.Desktop.AddIns.DockableWindow
        {
            private LinkMaps2DockWin m_windowUI;

            public AddinImpl()
            {
            }

            protected override IntPtr OnCreateChild()
            {
                m_windowUI = new LinkMaps2DockWin(this.Hook);
                return m_windowUI.Handle;
            }

            protected override void Dispose(bool disposing)
            {
                if (m_windowUI != null)
                    m_windowUI.Dispose(disposing);

                base.Dispose(disposing);
            }

        }
    }
}
