﻿// -----------------------------------------------------------------------
// File: AddLayerButton.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using System;
    using System.Windows.Forms;

    using ESRI.ArcGIS.ADF.BaseClasses;
    using ESRI.ArcGIS.Carto;
    using ESRI.ArcGIS.Catalog;
    using ESRI.ArcGIS.CatalogUI;
    using ESRI.ArcGIS.DataSourcesFile;
    using ESRI.ArcGIS.DataSourcesRaster;
    using ESRI.ArcGIS.Display;
    using ESRI.ArcGIS.Geodatabase;
    using ESRI.ArcGIS.Geometry;

    using LinkMaps2.Properties;

    using Path = System.IO.Path;

    /// <summary>
    /// Button to add a layer to the map in the dockable window.
    /// </summary>
    public class AddLayerButton : BaseCommand
    {
        public AddLayerButton()
        {
            base.m_caption = "Add Layer";
            base.m_message = "Add a new layer to the map.";
            base.m_bitmap = Resources.DataAdd16;
        }

        public override void OnClick()
        {
            try
            {
                // Set up the dialog and filters that determine the types of files the user can select
                IGxObjectFilter rasterFilter = new GxFilterRasterDatasetsClass();
                IGxObjectFilter shapefileFilter = new GxFilterShapefilesClass();
                IGxDialog dlg = new GxDialogClass();
                var filters = (IGxObjectFilterCollection)dlg;
                filters.AddFilter(rasterFilter, false);
                filters.AddFilter(shapefileFilter, false);
                dlg.Title = "Load one or more datasets";
                dlg.ButtonCaption = "Load";
                dlg.AllowMultiSelect = true;

                // Let the user select layers; return if they cancelled out
                IEnumGxObject objects;
                if (!dlg.DoModalOpen(0, out objects))
                {
                    return;
                }

                // Loop through the list if selected layers and add them to the map control
                IGxObject obj;
                while ((obj = objects.Next()) != null)
                {
                    var dataset = (IGxDataset)obj;
                    if (dataset.Type == esriDatasetType.esriDTRasterDataset)
                    {
                        AddLayerButton.AddRaster((IRasterDataset)dataset.Dataset, obj.FullName);
                    }
                    else if (dataset.Type == esriDatasetType.esriDTFeatureClass)
                    {
                        AddLayerButton.AddVector((IFeatureClass)dataset.Dataset, obj.FullName);
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(
                    "Unable to add layer: " + e.Message,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public override void OnCreate(object hook)
        {
        }

        /// <summary>
        /// Adds a raster layer to the dockable window's map.
        /// </summary>
        /// <param name="path">full file path to the raster</param>
        internal static void AddRaster(string path)
        {
            IWorkspaceFactory factory = new RasterWorkspaceFactoryClass();
            var ws = (IRasterWorkspace)factory.OpenFromFile(Path.GetDirectoryName(path), 0);
            IRasterDataset dataset = ws.OpenRasterDataset(Path.GetFileName(path));
            AddLayerButton.AddRaster(dataset, path);
        }

        /// <summary>
        /// Adds a vector layer to the dockable window's map.
        /// </summary>
        /// <param name="path">full file path the dataset</param>
        internal static void AddVector(string path)
        {
            IWorkspaceFactory factory = new ShapefileWorkspaceFactoryClass();
            var ws = (IFeatureWorkspace)factory.OpenFromFile(Path.GetDirectoryName(path), 0);
            IFeatureClass fc = ws.OpenFeatureClass(Path.GetFileNameWithoutExtension(path));
            AddLayerButton.AddVector(fc, path);
        }

        /// <summary>
        /// Sets up symbology for a raster layer and adds it to the dockable window's map.
        /// </summary>
        /// <param name="dataset">dataset to make the layer from</param>
        /// <param name="path">full file path to the raster</param>
        private static void AddRaster(IRasterDataset dataset, string path)
        {
            // Make sure the dataset has statistics and pyramids
            PrepareRasterDataset(dataset);

            // Create the layer
            IRasterLayer rasterLayer = new RasterLayerClass();
            rasterLayer.CreateFromDataset(dataset);

            // Make sure that the layer has enough bands so the renderer doesn't crash
            if (rasterLayer.BandCount > 1 && rasterLayer.BandCount < 5)
            {
                throw new Exception("Raster layers must have either one or at least 5 bands.");
            }

            // Apply a 3-band renderer if needed
            if (rasterLayer.BandCount > 1)
            {
                IRasterRGBRenderer2 rasterRenderer = new RasterRGBRendererClass();
                rasterRenderer.SetBandIndices(4, 3, 1);
                rasterRenderer.UseAlphaBand = false;
                ((IRasterStretch2)rasterRenderer).StretchType =
                    esriRasterStretchTypesEnum.esriRasterStretch_MinimumMaximum;
                rasterLayer.Renderer = (IRasterRenderer)rasterRenderer;
            }

            // Add the layer
            LinkMaps2DockWin.AddRaster(rasterLayer, path);
        }

        /// <summary>
        /// Sets up symbology for a vector layer and adds it to the dockable window's map.
        /// </summary>
        /// <param name="featureClass">feature class to make the layer from</param>
        /// <param name="path">full file path to the dataset</param>
        private static void AddVector(IFeatureClass featureClass, string path)
        {
            // Check that it is a polygon layer
            if (featureClass.ShapeType != esriGeometryType.esriGeometryPolygon)
            {
                throw new Exception("Only polygon feature layers are supported at this time");
            }

            // Create a polygon renderer that is hollow with yellow outlines
            IRgbColor yellow = new RgbColorClass();
            yellow.Red = 255;
            yellow.Green = 255;
            yellow.Blue = 0;
            ILineSymbol lineSymbol = new SimpleLineSymbolClass();
            lineSymbol.Color = yellow;
            ISimpleFillSymbol fillSymbol = new SimpleFillSymbolClass();
            fillSymbol.Style = esriSimpleFillStyle.esriSFSHollow;
            fillSymbol.Outline = lineSymbol;
            ISimpleRenderer shapefileRenderer = new SimpleRendererClass();
            shapefileRenderer.Symbol = (ISymbol)fillSymbol;

            // Create the layer and set the renderer
            IFeatureLayer featureLayer = new FeatureLayerClass();
            featureLayer.FeatureClass = featureClass;
            featureLayer.Name = featureClass.AliasName;
            var geoLayer = (IGeoFeatureLayer)featureLayer;
            geoLayer.Renderer = (IFeatureRenderer)shapefileRenderer;

            // Set up the labeling
            geoLayer.DisplayAnnotation = true;
            IAnnotateLayerProperties labelProperties;
            IElementCollection placed;
            IElementCollection unplaced;
            geoLayer.AnnotationProperties.QueryItem(0, out labelProperties, out placed, out unplaced);
            var engineProperties = (ILabelEngineLayerProperties)labelProperties;
            engineProperties.Symbol.Color = yellow;
            engineProperties.Expression = "[FID]";

            // Add the layer
            LinkMaps2DockWin.AddVector(featureLayer, path);
        }

        /// <summary>
        /// Makes sure a raster dataset has statistics and pyramids so it can be shown in the map
        /// control.
        /// </summary>
        /// <param name="dataset">raster dataset to check</param>
        private static void PrepareRasterDataset(IRasterDataset dataset)
        {
            // Calculate statistics for each band if needed.
            var bands = (IRasterBandCollection)dataset;
            for (int i = 0; i < bands.Count; i++)
            {
                IRasterBand band = bands.Item(i);
                bool hasStats;
                band.HasStatistics(out hasStats);
                if (!hasStats)
                {
                    LinkMaps2DockWin.ShowMessage(
                        string.Format("Calculating statistics for band {0}...", i + 1));
                    band.ComputeStatsAndHist();
                }
            }

            // Build pyramids if needed.
            var pyramids = (IRasterPyramid)dataset;
            if (!pyramids.Present)
            {
                LinkMaps2DockWin.ShowMessage("Building pyramids...");
                pyramids.Create();
            }

            LinkMaps2DockWin.ShowMessage(null);
        }
    }
}
