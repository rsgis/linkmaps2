﻿// -----------------------------------------------------------------------
// File: ZoomToLayerCommand.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using ESRI.ArcGIS.ADF.BaseClasses;
    using ESRI.ArcGIS.Carto;
    using ESRI.ArcGIS.Controls;

    using LinkMaps2.Properties;

    /// <summary>
    /// Context menu command to zoom to the selected layer.
    /// </summary>
    public class ZoomToLayerCommand : BaseCommand
    {
        private ITOCControl toc;

        public ZoomToLayerCommand()
        {
            base.m_caption = "Zoom to layer";
            base.m_bitmap = Resources.LayerZoomTo16;
        }

        public override void OnClick()
        {
            LinkMaps2DockWin.ZoomToExtent(((ILayer)this.toc.CustomProperty).AreaOfInterest);
        }

        public override void OnCreate(object hook)
        {
            this.toc = (ITOCControl)hook;
        }
    }
}