﻿// -----------------------------------------------------------------------
// File: ClearCommand.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-04-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using ESRI.ArcGIS.ADF.BaseClasses;

    using LinkMaps2.Properties;

    /// <summary>
    /// Context menu command to remove all layers from the map.
    /// </summary>
    public class ClearCommand : BaseCommand
    {
        public ClearCommand()
        {
            base.m_caption = "Remove all layers";
            base.m_bitmap = Resources.GenericBlackDelete16;
        }

        public override void OnClick()
        {
            LinkMaps2DockWin.Clear();
        }

        public override void OnCreate(object hook)
        {
        }
    }
}