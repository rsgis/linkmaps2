﻿// -----------------------------------------------------------------------
// File: LayerVisibilityCommand.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-04-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using ESRI.ArcGIS.ADF.BaseClasses;
    using ESRI.ArcGIS.Carto;
    using ESRI.ArcGIS.Controls;

    using LinkMaps2.Properties;

    /// <summary>
    /// Context menu command to toggle layer visibility.
    /// </summary>
    public class LayerVisibilityCommand : BaseCommand
    {
        private ITOCControl toc;

        public LayerVisibilityCommand()
        {
            base.m_caption = "Toggle visibility";
            base.m_bitmap = Resources.LayerGeneric16;
        }

        public override string Caption
        {
            get
            {
                // It all of the sudden starting asking for Caption before calling OnCreate() and I
                // have no idea why
                if (this.toc == null)
                {
                    return base.m_caption;
                }

                var layer = (ILayer)this.toc.CustomProperty;
                if (layer is IRasterLayer)
                {
                    return "Make visible raster";
                }

                return layer.Visible ? "Hide" : "Show";
            }
        }

        public override bool Enabled
        {
            get
            {
                var layer = (ILayer)this.toc.CustomProperty;
                return layer is IFeatureLayer || !layer.Visible;
            }
        }

        public override void OnClick()
        {
            LinkMaps2DockWin.ToggleVisibility((ILayer)this.toc.CustomProperty);
        }

        public override void OnCreate(object hook)
        {
            this.toc = (ITOCControl)hook;
        }

        public void UpdateBitmap()
        {
            if (this.toc == null)
            {
                return;
            }

            var layer = (ILayer)this.toc.CustomProperty;
            if (layer is IRasterLayer)
            {
                base.UpdateBitmap(Resources.LayerRaster16);
            }
            else if (layer.Visible)
            {
                base.UpdateBitmap(Resources.LayerGenericNotVisible16);
            }
            else
            {
                base.UpdateBitmap(Resources.LayerGeneric16);
            }
        }
    }
}