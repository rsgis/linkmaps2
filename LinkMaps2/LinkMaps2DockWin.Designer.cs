﻿namespace LinkMaps2
{
    partial class LinkMaps2DockWin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkMaps2DockWin));
            this.linkSplitContainer = new System.Windows.Forms.SplitContainer();
            this.tocControl = new ESRI.ArcGIS.Controls.AxTOCControl();
            this.mapControl = new ESRI.ArcGIS.Controls.AxMapControl();
            this.linkToolbarControl = new ESRI.ArcGIS.Controls.AxToolbarControl();
            this.currentRasterLabel = new System.Windows.Forms.Label();
            this.messageLabel = new System.Windows.Forms.Label();
            this.linkSplitContainer.Panel1.SuspendLayout();
            this.linkSplitContainer.Panel2.SuspendLayout();
            this.linkSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tocControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkToolbarControl)).BeginInit();
            this.SuspendLayout();
            // 
            // linkSplitContainer
            // 
            this.linkSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkSplitContainer.Location = new System.Drawing.Point(3, 37);
            this.linkSplitContainer.Name = "linkSplitContainer";
            // 
            // linkSplitContainer.Panel1
            // 
            this.linkSplitContainer.Panel1.Controls.Add(this.tocControl);
            // 
            // linkSplitContainer.Panel2
            // 
            this.linkSplitContainer.Panel2.Controls.Add(this.mapControl);
            this.linkSplitContainer.Size = new System.Drawing.Size(592, 558);
            this.linkSplitContainer.SplitterDistance = 197;
            this.linkSplitContainer.TabIndex = 0;
            // 
            // tocControl
            // 
            this.tocControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tocControl.Location = new System.Drawing.Point(0, 0);
            this.tocControl.Name = "tocControl";
            this.tocControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tocControl.OcxState")));
            this.tocControl.Size = new System.Drawing.Size(197, 558);
            this.tocControl.TabIndex = 0;
            this.tocControl.OnMouseDown += new ESRI.ArcGIS.Controls.ITOCControlEvents_Ax_OnMouseDownEventHandler(this.tocControl_OnMouseDown);
            // 
            // mapControl
            // 
            this.mapControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl.Location = new System.Drawing.Point(0, 0);
            this.mapControl.Name = "mapControl";
            this.mapControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("mapControl.OcxState")));
            this.mapControl.Size = new System.Drawing.Size(391, 558);
            this.mapControl.TabIndex = 0;
            this.mapControl.OnKeyUp += new ESRI.ArcGIS.Controls.IMapControlEvents2_Ax_OnKeyUpEventHandler(this.mapControl_OnKeyUp);
            // 
            // linkToolbarControl
            // 
            this.linkToolbarControl.Location = new System.Drawing.Point(4, 4);
            this.linkToolbarControl.Name = "linkToolbarControl";
            this.linkToolbarControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("linkToolbarControl.OcxState")));
            this.linkToolbarControl.Size = new System.Drawing.Size(265, 28);
            this.linkToolbarControl.TabIndex = 2;
            // 
            // currentRasterLabel
            // 
            this.currentRasterLabel.AutoSize = true;
            this.currentRasterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentRasterLabel.Location = new System.Drawing.Point(275, 10);
            this.currentRasterLabel.Name = "currentRasterLabel";
            this.currentRasterLabel.Size = new System.Drawing.Size(80, 17);
            this.currentRasterLabel.TabIndex = 3;
            this.currentRasterLabel.Text = "raster layer";
            this.currentRasterLabel.Click += new System.EventHandler(this.currentRasterLabel_Click);
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(361, 10);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(65, 17);
            this.messageLabel.TabIndex = 4;
            this.messageLabel.Text = "message";
            this.messageLabel.Visible = false;
            // 
            // LinkMaps2DockWin
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.currentRasterLabel);
            this.Controls.Add(this.linkToolbarControl);
            this.Controls.Add(this.linkSplitContainer);
            this.Name = "LinkMaps2DockWin";
            this.Size = new System.Drawing.Size(598, 598);
            this.VisibleChanged += new System.EventHandler(this.LinkMaps2DockWin_VisibleChanged);
            this.linkSplitContainer.Panel1.ResumeLayout(false);
            this.linkSplitContainer.Panel2.ResumeLayout(false);
            this.linkSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tocControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linkToolbarControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer linkSplitContainer;
        private ESRI.ArcGIS.Controls.AxMapControl mapControl;
        private ESRI.ArcGIS.Controls.AxTOCControl tocControl;
        private ESRI.ArcGIS.Controls.AxToolbarControl linkToolbarControl;
        private System.Windows.Forms.Label currentRasterLabel;
        private System.Windows.Forms.Label messageLabel;

    }
}
