﻿// -----------------------------------------------------------------------
// File: ToggleDockWin.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using ESRI.ArcGIS.Framework;

    using Button = ESRI.ArcGIS.Desktop.AddIns.Button;

    /// <summary>
    /// Button to toggle the visibility of the dockable window.
    /// </summary>
    public class ToggleDockWin : Button
    {
        public ToggleDockWin()
        {
        }

        /// <summary>
        /// Toggles the visibility of the dockable window when the button is clicked.
        /// </summary>
        protected override void OnClick()
        {
            IDockableWindow dockWin = LinkMaps2Extension.GetDockWindow();
            if (dockWin == null)
            {
                return;
            }

            dockWin.Show(!dockWin.IsVisible());
            if (dockWin.IsVisible())
            {
                LinkMaps2Extension.GetExtension().Listen(true);
                LinkMaps2Extension.GetExtension().CheckSyncing();
            }
        }

        /// <summary>
        /// The button is always enabled as long as there is a valid ArcMap application.
        /// </summary>
        protected override void OnUpdate()
        {
            this.Enabled = ArcMap.Application != null;
        }
    }
}