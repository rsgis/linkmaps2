﻿// -----------------------------------------------------------------------
// File: NextButton.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using System;

    using ESRI.ArcGIS.ADF.BaseClasses;

    using LinkMaps2.Properties;

    /// <summary>
    /// Button to change the visible raster to the one after the currently visible one.
    /// </summary>
    public class NextButton : BaseCommand
    {
        public NextButton()
        {
            base.m_caption = "Next Raster";
            base.m_message = "Show next raster layer.";
            base.m_bitmap = Resources.GenericBlackRightArrowNoTail16;
        }

        public override bool Enabled
        {
            get { return LinkMaps2DockWin.RasterCount > 1; }
        }

        public override void OnClick()
        {
            LinkMaps2DockWin.ShowNextRaster();
        }

        public override void OnCreate(object hook)
        {
        }
    }
}