﻿// -----------------------------------------------------------------------
// File: ToggleTocButton.cs
// Project: LinkMaps2
// Remote Sensing / GIS Laboratory
// Quinney College of Natural Resources, Utah State University
// Last modified on 04-03-2013 by chrisg 
// -----------------------------------------------------------------------
namespace LinkMaps2
{
    using System;

    using ESRI.ArcGIS.ADF.BaseClasses;

    using LinkMaps2.Properties;

    /// <summary>
    /// Button to toggle the TOC visibility on the dockable window.
    /// </summary>
    public class ToggleTocButton : BaseCommand
    {
        public ToggleTocButton()
        {
            base.m_caption = "Toggle TOC";
            base.m_message = "Toogle visibility of the TOC.";
            base.m_bitmap = Resources.Legend16;
        }

        public override void OnClick()
        {
            LinkMaps2DockWin.ToggleToc();
        }

        public override void OnCreate(object hook)
        {
        }
    }
}